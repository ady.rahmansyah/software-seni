import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setRandomBg } from "../store/pokemon/action";

type PokemonType = {
  name?: string;
  url?: string;
  id?: any;
};

type PokemonSearchType = {
  pokemons: PokemonType[];
};

const Home = () => {
  const [keyword, setKeyword] = useState<string>("");
  const [pokemons, setPokemons] = useState<any>([]);

  useEffect(() => {
    getAllPokemons();
  }, []);

  const getAllPokemons = async () => {
    await axios
      .get(`${process.env.REACT_APP_BASE_URL}pokemon?limit=24`)
      .then((res) => setPokemons(res.data.results))
      .catch((err) => console.log(err));
  };

  const searchPokemon = async (e: any) => {
    e.preventDefault();
    keyword !== ""
      ? await axios
          .get(`${process.env.REACT_APP_BASE_URL}pokemon/${keyword}`)
          .then((res) => setPokemons([res.data]))
          .catch((err) => {
            console.log(err);
            setPokemons([]);
          })
      : getAllPokemons();
  };

  return (
    <div className="container mx-auto px-2 lg:px-0 py-8">
      <h1 className="text-3xl font-bold px-2 mb-4">Pokemon List</h1>
      <form className="px-2 mb-8 flex items-center">
        <input
          type="text"
          placeholder="Search Pokemon"
          className="border p-2 rounded w-full"
          onChange={(e) => setKeyword(e.target.value)}
        />
        <button
          type="submit"
          className="ml-2 text-sm bg-green-500 h-full px-4 py-2.5 text-white rounded"
          onClick={searchPokemon}
        >
          Search
        </button>
      </form>
      {pokemons.length > 0 ? (
        <PokemonList pokemons={pokemons} />
      ) : (
        <div className="pl-2">Data tidak ditemukan!</div>
      )}
    </div>
  );
};

const PokemonList = (props: PokemonSearchType) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [bgcolor] = useState(
    props.pokemons.map((data) =>
      Math.floor(Math.random() * 16777215).toString(16)
    )
  );
  const image = props.pokemons.map((data) => {
    return props.pokemons.length === 1 ? data.id : data.url?.split("/");
  });

  const pokemonDetail = (data: PokemonType, bgcolor: string) => {
    const detail = data.url?.split("/");
    navigate(
      `/pokemon-detail/${
        // @ts-ignore
        props.pokemons.length > 1 ? detail[detail.length - 2] : data.id
      }`
    );
    dispatch(setRandomBg(bgcolor));
  };

  const indexing = (number: any, length: number) => {
    let str = "" + number;
    while (str.length < length) {
      str = "0" + str;
    }
    return str;
  };

  return (
    <div className="flex flex-wrap">
      {props.pokemons &&
        props.pokemons.map((data, idx) => (
          <div key={idx} className="p-2 w-1/2 md:w-1/3 lg:w-1/4">
            <div
              className="border shadow-lg rounded-lg p-2 h-40 flex flex-col justify-center items-center justify-center uppercase"
              onClick={() => pokemonDetail(data, bgcolor[idx])}
              style={{ backgroundColor: `#${bgcolor[idx]}` }}
            >
              <img
                src={`https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${indexing(
                  props.pokemons.length === 1 ? data.id : image[idx][6],
                  3
                )}.png`}
                alt={data.name}
                width="100"
              />
              <h3 className="text-lg lg:text-2xl font-bold text-white">
                {data.name}
              </h3>
            </div>
          </div>
        ))}
    </div>
  );
};
export default Home;
