import { useState, useEffect } from "react";
import axios from "axios";
import { useParams } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setRandomBg } from "../store/pokemon/action";

const PokemonDetail = () => {
  const [pokemon, setPokemon] = useState<any>("");
  const params = useParams();
  const bgcolor = useSelector((state: any) => state.pokemon.bgcolor);
  const dispatch = useDispatch();

  useEffect(() => {
    getPokemonDetail(); // eslint-disable-next-line react-hooks/exhaustive-deps
    console.log(bgcolor);
    return () => {
      dispatch(setRandomBg(bgcolor));
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getPokemonDetail = async () => {
    await axios
      .get(`${process.env.REACT_APP_BASE_URL}pokemon/${params.id}`)
      .then((res) => setPokemon(res.data))
      .catch((err) => console.log(err));
  };

  const indexing = (number: any, length: number) => {
    let str = "" + number;
    while (str.length < length) {
      str = "0" + str;
    }
    return str;
  };

  return (
    <div
      className="min-h-screen"
      style={{
        backgroundColor: `#${bgcolor}`,
      }}
    >
      <div className="container mx-auto lg:px-0 px-4 py-8">
        <div className="mx-auto lg:w-1/3 md:w-1/2 w-full shadow-xl rounded-lg border-2 overflow-hidden">
          <div className="text-2xl font-bold uppercase mb-4 text-white pl-4 pt-4">
            {pokemon.name}
          </div>
          <div className="flex pl-4">
            {pokemon &&
              pokemon.types.map((data: any, idx: number) => (
                <div
                  key={idx}
                  className="bg-white mr-2 px-3 py-1 rounded-full capitalize text-sm border-2 shadow"
                  style={{ color: `#${bgcolor}` }}
                >
                  {data.type.name}
                </div>
              ))}
          </div>
          <div className="flex justify-center">
            <img
              src={`https://assets.pokemon.com/assets/cms2/img/pokedex/detail/${indexing(
                params.id,
                3
              )}.png`}
              alt={pokemon.name}
              width="100%"
            />
          </div>
          <div className="py-8 bg-white shadow-lg p-4">
            <div className="flex mb-2">
              <div className="w-20">Height</div>
              <span className="ml-2">: {pokemon.height} inch</span>
            </div>
            <div className="flex mb-2">
              <div className="w-20">Weight</div>
              <span className="ml-2">: {pokemon.weight} lbs</span>
            </div>
            <div className="flex mb-2">
              <div className="w-20">Abilities</div>
              <span className="ml-2 capitalize">
                <span className="mr-1">:</span>
                {pokemon &&
                  pokemon.abilities.map(
                    (data: any, idx: number) =>
                      data.ability.name +
                      (idx !== pokemon.abilities.length - 1 ? ", " : "")
                  )}
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PokemonDetail;
