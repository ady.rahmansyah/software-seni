import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "../pages/Home";
import PokemonDetail from "../pages/PokemonDetail";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/pokemon-detail/:id" element={<PokemonDetail />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
