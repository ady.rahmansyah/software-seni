import { GET_POKEMONS, GET_POKEMON_DETAIL, SET_RANDOM_BG } from "../constants";

const pokemons: any = {
  lists: [],
  url: "",
  bgcolor: "#000000",
};

const pokemonReducer = (state = pokemons, action: any) => {
  switch (action.type) {
    case GET_POKEMONS:
      return { ...pokemons, lists: action.payload };
    case GET_POKEMON_DETAIL:
      return { ...pokemons, url: action.payload };
    case SET_RANDOM_BG:
      return { ...pokemons, bgcolor: action.payload };
    default:
      return state;
  }
};

export default pokemonReducer;
