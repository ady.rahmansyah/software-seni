import { GET_POKEMONS, GET_POKEMON_DETAIL, SET_RANDOM_BG } from "../constants";

export const getPokemons = (data: any) => {
  return {
    type: GET_POKEMONS,
    payload: data,
  };
};

export const getPokemonDetail = (data: string) => {
  return {
    type: GET_POKEMON_DETAIL,
    payload: data,
  };
};

export const setRandomBg = (data: string) => {
  return {
    type: SET_RANDOM_BG,
    payload: data,
  };
};
